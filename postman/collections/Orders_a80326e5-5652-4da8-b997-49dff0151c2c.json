{
    "info": {
        "_postman_id": "ac78c6d0-9122-46b6-8d08-0e9e49e0a007",
        "name": "Orders",
        "description": "# Orders - Endpoints for managing a user's order\n<img src=\"https://www.pngkit.com/png/detail/299-2992841_complete-order-comments-order-icon-png-transparent.png\" alt=\"drawing\" width=\"30\" height=\"30\"/></img>\n\nThis collection includes api's for managing and updating an order during its finalization and after purchase. Prior to completing the purchase, a user can update information associated with the order including shipping and billing addresses as well as payment method. Submitting an order to purchase will complete the purchase process, permanently associate the relevant basket information with the order, and delete the associated basket.\n\nOrders are created from the `Submit to order` endpoint in the [Basket](https://retail-demo.postman.co/workspace/100e5a1b-07cd-4b14-b2e1-df8da7d81ae2/collection/16340833-02074e01-b519-4667-b1ac-92ece5621802?ctx=documentation) collection",
        "schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
    },
    "item": [
        {
            "name": "Get all orders by userId",
            "event": [
                {
                    "listen": "test",
                    "script": {
                        "id": "117776a4-5e89-4b97-a903-76c239147c6a",
                        "exec": [
                            "var jsonData = pm.response.json();",
                            "",
                            "pm.test(\"Status code is 200\", function () {",
                            "    pm.response.to.have.status(200);",
                            "});",
                            "",
                            "pm.test(\"Expect an orders array\", function () {",
                            "    pm.expect(jsonData).to.have.property(\"orders\");",
                            "});",
                            "",
                            "pm.test(\"Expect orders to have an ID and items\", function () {",
                            "    for (var i = 0; i < jsonData.orders.length; i++) {",
                            "        pm.expect(jsonData.orders[i]).to.include.all.keys(\"id\", \"items\");",
                            "    }",
                            "});",
                            "",
                            "pm.test(\"Expect item ID to be string\", function () {",
                            "    for (var i = 0; i < jsonData.orders.length; i++) {",
                            "        pm.expect(jsonData.orders[i].id).to.be.a('string');",
                            "    }",
                            "});",
                            "",
                            "pm.test(\"Expect items to be an array\", function () {",
                            "    for (var i = 0; i < jsonData.orders.length; i++) {",
                            "        pm.expect(jsonData.orders[i].items).to.be.an('array');",
                            "    }",
                            "});"
                        ],
                        "type": "text/javascript"
                    }
                }
            ],
            "id": "a4cfc8b7-de4c-4d52-935d-5b2f65a85ffe",
            "protocolProfileBehavior": {
                "disableBodyPruning": true
            },
            "request": {
                "auth": {
                    "type": "noauth"
                },
                "method": "GET",
                "header": [],
                "url": {
                    "raw": "{{ordersUrl}}/orders?userId=MTU1MDg2MTI2MDQ0Mg==",
                    "host": [
                        "{{ordersUrl}}"
                    ],
                    "path": [
                        "orders"
                    ],
                    "query": [
                        {
                            "description": "A starting UNIX timestamp.",
                            "key": "startDate",
                            "value": "{{$timestamp}}",
                            "disabled": true
                        },
                        {
                            "description": "An ending UNIX timestamp.",
                            "key": "endDate",
                            "value": "{{$timestamp}}",
                            "disabled": true
                        },
                        {
                            "key": "userId",
                            "value": "MTU1MDg2MTI2MDQ0Mg==",
                            "description": "The ID of the user who&amp;amp;amp;amp;amp;amp;#39;s orders should be returned."
                        }
                    ]
                },
                "description": "Pulls a list of all orders"
            },
            "response": [
                {
                    "id": "1452da65-43a2-4ebf-9085-94de1a3921bf",
                    "name": "200 - Success",
                    "originalRequest": {
                        "method": "GET",
                        "header": [],
                        "url": {
                            "raw": "{{ordersUrl}}/orders?userId=MTU1MDg2MTI2MDQ0Mg==",
                            "host": [
                                "{{ordersUrl}}"
                            ],
                            "path": [
                                "orders"
                            ],
                            "query": [
                                {
                                    "key": "start",
                                    "value": "",
                                    "disabled": true
                                },
                                {
                                    "key": "end",
                                    "value": "<string>",
                                    "disabled": true
                                },
                                {
                                    "key": "userId",
                                    "value": "MTU1MDg2MTI2MDQ0Mg=="
                                }
                            ]
                        }
                    },
                    "status": "OK",
                    "code": 200,
                    "_postman_previewlanguage": "Text",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json"
                        }
                    ],
                    "cookie": [],
                    "responseTime": null,
                    "body": "{\n    \"orders\": [\n        {\n            \"id\": \"1550861260442-MTU1MDg2MTI2MDQ0Mg==\",\n            \"items\": [\n                {\n                \"id\": \"XYZ-JEAN-123\",\n                \"quantity\": 2\n                }\n            ],\n            \"shippingAddress\": \n                {\n                    \"address1\":     \"1313 Mockingbird Ln.\",\n                    \"address2\":     \"Apt E.\",\n                    \"city\":         \"Wilmington\",\n                    \"state\":        \"DE\",\n                    \"zip\":          \"00000\"\n                },\n            \"billingAddress\": \n                {\n                    \"address1\":     \"1313 Mockingbird Ln.\",\n                    \"address2\": \t\"Apt E.\",\n                    \"city\":\t\t    \"Wilmington\",\n                    \"state\":        \"DE\",\n                    \"zip\":          \"00000\"\n                },\n            \"paymentType\":      \"stored\",\n            \"storedPaymentID\":\t1\n        }\n    ]\n}"
                },
                {
                    "id": "f0d04db7-0ca4-4d8e-a0de-b0c57c8dd639",
                    "name": "404 - User not found",
                    "originalRequest": {
                        "method": "GET",
                        "header": [],
                        "url": {
                            "raw": "{{ordersUrl}}/orders?userId=MTU1MDg2MTI2MDQ0Mg==",
                            "host": [
                                "{{ordersUrl}}"
                            ],
                            "path": [
                                "orders"
                            ],
                            "query": [
                                {
                                    "key": "userId",
                                    "value": "MTU1MDg2MTI2MDQ0Mg=="
                                }
                            ]
                        }
                    },
                    "status": "Not Found",
                    "code": 404,
                    "_postman_previewlanguage": "Text",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json"
                        }
                    ],
                    "cookie": [],
                    "responseTime": null,
                    "body": "{\n    \"status\":   \"failed\",\n    \"message\":  \"User does not exist\"\n}"
                }
            ]
        },
        {
            "name": "Submit to purchase",
            "event": [
                {
                    "listen": "test",
                    "script": {
                        "id": "35a660a6-a4d6-4f3f-9755-c7a7d78922df",
                        "exec": [
                            "pm.test(\"Status code is 200\", function () {",
                            "    pm.response.to.have.status(200);",
                            "});"
                        ],
                        "type": "text/javascript"
                    }
                }
            ],
            "id": "f631f402-f8d9-40b1-a118-aa35d1a2dc9b",
            "protocolProfileBehavior": {
                "disableBodyPruning": true
            },
            "request": {
                "auth": {
                    "type": "noauth"
                },
                "method": "GET",
                "header": [],
                "url": {
                    "raw": "{{ordersUrl}}/orders/submit_to_purchase?id=1550861260442-MTU1MDg2MTI2MDQ0Mg=-",
                    "host": [
                        "{{ordersUrl}}"
                    ],
                    "path": [
                        "orders",
                        "submit_to_purchase"
                    ],
                    "query": [
                        {
                            "key": "id",
                            "value": "1550861260442-MTU1MDg2MTI2MDQ0Mg=-"
                        }
                    ]
                },
                "description": "Submits an order to be purchased."
            },
            "response": [
                {
                    "id": "bc7f98d7-4336-48be-a9f3-514a9d981018",
                    "name": "200 - Purchase created",
                    "originalRequest": {
                        "method": "GET",
                        "header": [],
                        "url": {
                            "raw": "{{ordersUrl}}/orders/submit_to_purchase?id=1550861260442-MTU1MDg2MTI2MDQ0Mg=-",
                            "host": [
                                "{{ordersUrl}}"
                            ],
                            "path": [
                                "orders",
                                "submit_to_purchase"
                            ],
                            "query": [
                                {
                                    "key": "id",
                                    "value": "1550861260442-MTU1MDg2MTI2MDQ0Mg=-"
                                }
                            ]
                        }
                    },
                    "status": "OK",
                    "code": 200,
                    "_postman_previewlanguage": "Text",
                    "header": [],
                    "cookie": [],
                    "responseTime": null,
                    "body": ""
                },
                {
                    "id": "a094e6e9-37bd-4709-ac99-2510f7334bc5",
                    "name": "404 - Order not found",
                    "originalRequest": {
                        "method": "GET",
                        "header": [],
                        "url": {
                            "raw": "{{ordersUrl}}/orders/submit_to_purchase?id=1550861260442-MTU1MDg2MTI2MDQ0Mg==",
                            "host": [
                                "{{ordersUrl}}"
                            ],
                            "path": [
                                "orders",
                                "submit_to_purchase"
                            ],
                            "query": [
                                {
                                    "key": "id",
                                    "value": "1550861260442-MTU1MDg2MTI2MDQ0Mg=="
                                }
                            ]
                        }
                    },
                    "status": "Not Found",
                    "code": 404,
                    "_postman_previewlanguage": "Text",
                    "header": [],
                    "cookie": [],
                    "responseTime": null,
                    "body": ""
                },
                {
                    "id": "322ce7d1-1b69-4073-9af1-16a24b8f27c0",
                    "name": "409 - Order not modifiable",
                    "originalRequest": {
                        "method": "GET",
                        "header": [],
                        "url": {
                            "raw": "{{ordersUrl}}/orders/submit_to_purchase?id=1550861260442-MTU1MDg2MTI2MDQ0Mg==",
                            "host": [
                                "{{ordersUrl}}"
                            ],
                            "path": [
                                "orders",
                                "submit_to_purchase"
                            ],
                            "query": [
                                {
                                    "key": "id",
                                    "value": "1550861260442-MTU1MDg2MTI2MDQ0Mg=="
                                }
                            ]
                        }
                    },
                    "status": "Conflict",
                    "code": 409,
                    "_postman_previewlanguage": "Text",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json"
                        }
                    ],
                    "cookie": [],
                    "responseTime": null,
                    "body": "{\n    \"status\":   \"failed\",\n    \"message\":  \"Order no longer modifiable\"\n}"
                }
            ]
        },
        {
            "name": "Update order",
            "event": [
                {
                    "listen": "test",
                    "script": {
                        "id": "4665ac00-abdf-4183-9801-67c8061b22bd",
                        "exec": [
                            "pm.test(\"Status code is 200\", function () {",
                            "    pm.response.to.have.status(200);",
                            "});"
                        ],
                        "type": "text/javascript"
                    }
                }
            ],
            "id": "1d76180b-34eb-4103-bada-56f3eefcb464",
            "protocolProfileBehavior": {
                "disableBodyPruning": true
            },
            "request": {
                "auth": {
                    "type": "noauth"
                },
                "method": "PUT",
                "header": [
                    {
                        "key": "Content-Type",
                        "value": "application/json"
                    }
                ],
                "body": {
                    "mode": "raw",
                    "raw": "{\n    \"shippingAddress\": \n            {\n                \"address1\":     \"1313 Mockingbird Ln.\",\n                \"address2\":     \"Apt E.\",\n                \"city\":         \"Wilmington\",\n                \"state\":        \"DE\",\n                \"zip\":          \"00000\"\n            },\n        \"billingAddress\": \n            {\n                \"address1\":     \"1313 Mockingbird Ln.\",\n                \"address2\": \t\"Apt E.\",\n                \"city\":\t\t    \"Wilmington\",\n                \"state\":        \"DE\",\n                \"zip\":          \"00000\"\n            },\n\t\"paymentType\":      \"stored\",\n\t\"storedPaymentID\":\t0\n}\n"
                },
                "url": {
                    "raw": "{{ordersUrl}}/orders/:orderId",
                    "host": [
                        "{{ordersUrl}}"
                    ],
                    "path": [
                        "orders",
                        ":orderId"
                    ],
                    "variable": [
                        {
                            "id": "9f2da472-6662-4795-9eef-49833b80650e",
                            "key": "orderId",
                            "value": "1550861260442-MTU1MDg2MTI2MDQ0Mg=-"
                        }
                    ]
                },
                "description": "Change the quantity of items/an item for a given basket."
            },
            "response": [
                {
                    "id": "67e63fa5-e719-4370-b1cf-64e75a80ac99",
                    "name": "200 - OK",
                    "originalRequest": {
                        "method": "PUT",
                        "header": [
                            {
                                "key": "Content-Type",
                                "value": "<string>"
                            },
                            {
                                "key": "Content-Type",
                                "value": "application/json"
                            }
                        ],
                        "body": {
                            "mode": "raw",
                            "raw": "{\n    \"items\": [\n        {\n            \"id\": \"XYZ-JEAN-123\",\n            \"quantity\": 1\n        },\n        {\n            \"id\": \"ABC-TOP-789\",\n            \"quantity\": 3\n        }\n    ]\n}"
                        },
                        "url": {
                            "raw": "{{ordersUrl}}/orders/1550861260442-MTU1MDg2MTI2MDQ0Mg=-",
                            "host": [
                                "{{ordersUrl}}"
                            ],
                            "path": [
                                "orders",
                                "1550861260442-MTU1MDg2MTI2MDQ0Mg=-"
                            ]
                        }
                    },
                    "status": "OK",
                    "code": 200,
                    "_postman_previewlanguage": "Text",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json"
                        }
                    ],
                    "cookie": [],
                    "responseTime": null,
                    "body": ""
                },
                {
                    "id": "b475e543-9fc0-447e-ab52-538276212096",
                    "name": "404 - Order not found",
                    "originalRequest": {
                        "method": "PUT",
                        "header": [],
                        "url": {
                            "raw": "{{ordersUrl}}/orders/1550861260442-MTU1MDg2MTI2MDQ0Mg==",
                            "host": [
                                "{{ordersUrl}}"
                            ],
                            "path": [
                                "orders",
                                "1550861260442-MTU1MDg2MTI2MDQ0Mg=="
                            ]
                        }
                    },
                    "status": "Not Found",
                    "code": 404,
                    "_postman_previewlanguage": "Text",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json"
                        }
                    ],
                    "cookie": [],
                    "responseTime": null,
                    "body": ""
                },
                {
                    "id": "e6db5e0a-6d96-48f1-94d8-64087c7d37ef",
                    "name": "409 - Order not modifiable",
                    "originalRequest": {
                        "method": "PUT",
                        "header": [],
                        "url": {
                            "raw": "{{ordersUrl}}/orders/1550861260442-MTU1MDg2MTI2MDQ0Mg==",
                            "host": [
                                "{{ordersUrl}}"
                            ],
                            "path": [
                                "orders",
                                "1550861260442-MTU1MDg2MTI2MDQ0Mg=="
                            ]
                        }
                    },
                    "status": "Conflict",
                    "code": 409,
                    "_postman_previewlanguage": "Text",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json"
                        }
                    ],
                    "cookie": [],
                    "responseTime": null,
                    "body": "{\n    \"status\":   \"failed\",\n    \"message\":  \"Order not modifiable\"\n}"
                }
            ]
        },
        {
            "name": "Get order",
            "event": [
                {
                    "listen": "test",
                    "script": {
                        "id": "214024f3-b410-47bf-aa57-6230f07d6ad2",
                        "type": "text/javascript",
                        "exec": [
                            "var jsonData = pm.response.json();",
                            "",
                            "pm.test(\"Status code is 200\", function () {",
                            "    pm.response.to.have.status(200);",
                            "});",
                            "",
                            "pm.test(\"Expect there to be an orders element\", function () {",
                            "    pm.expect(jsonData).to.have.property(\"order\");",
                            "});",
                            "",
                            "pm.test(\"Expect orders to have an ID and items\", function () {",
                            "    for (var i = 0; i < jsonData.order.length; i++) {",
                            "        pm.expect(jsonData.orders[i]).to.include.all.keys(\"id\", \"items\");",
                            "    }",
                            "});",
                            "",
                            "pm.test(\"Expect item ID to be string\", function () {",
                            "    for (var i = 0; i < jsonData.order.length; i++) {",
                            "        pm.expect(jsonData.order.id).to.be.a('string');",
                            "    }",
                            "});",
                            "",
                            "pm.test(\"Expect items to be an array\", function () {",
                            "    for (var i = 0; i < jsonData.order.length; i++) {",
                            "        pm.expect(jsonData.order.items).to.be.an('array');",
                            "    }",
                            "});"
                        ]
                    }
                }
            ],
            "id": "bdf7d657-a6d7-4505-bc94-2b0bf691450a",
            "protocolProfileBehavior": {
                "disableBodyPruning": true
            },
            "request": {
                "auth": {
                    "type": "noauth"
                },
                "method": "GET",
                "header": [],
                "url": {
                    "raw": "{{ordersUrl}}/orders?id=1550861260442-MTU1MDg2MTI2MDQ0Mg==",
                    "host": [
                        "{{ordersUrl}}"
                    ],
                    "path": [
                        "orders"
                    ],
                    "query": [
                        {
                            "description": "The order ID to return.",
                            "key": "id",
                            "value": "1550861260442-MTU1MDg2MTI2MDQ0Mg=="
                        }
                    ]
                },
                "description": "Initiate a return. The order id persists for futher reference."
            },
            "response": [
                {
                    "id": "9d610bf1-927a-42d6-8bbc-ad161ab65c35",
                    "name": "200 - Success",
                    "originalRequest": {
                        "method": "GET",
                        "header": [],
                        "url": {
                            "raw": "{{ordersUrl}}/orders?id=1550861260442-MTU1MDg2MTI2MDQ0Mg==",
                            "host": [
                                "{{ordersUrl}}"
                            ],
                            "path": [
                                "orders"
                            ],
                            "query": [
                                {
                                    "key": "id",
                                    "value": "1550861260442-MTU1MDg2MTI2MDQ0Mg=="
                                }
                            ]
                        }
                    },
                    "status": "OK",
                    "code": 200,
                    "_postman_previewlanguage": "Text",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json"
                        }
                    ],
                    "cookie": [],
                    "responseTime": null,
                    "body": "{\n    \"order\": \n        {\n            \"id\": \"1550861260442-MTU1MDg2MTI2MDQ0Mg==\",\n            \"items\": [\n                {\n                \"id\": \"XYZ-JEAN-123\",\n                \"quantity\": 2\n                }\n            ],\n            \"shippingAddress\": \n                {\n                    \"address1\":     \"1313 Mockingbird Ln.\",\n                    \"address2\":     \"Apt E.\",\n                    \"city\":         \"Wilmington\",\n                    \"state\":        \"DE\",\n                    \"zip\":          \"00000\"\n                },\n            \"billingAddress\": \n                {\n                    \"address1\":     \"1313 Mockingbird Ln.\",\n                    \"address2\": \t\"Apt E.\",\n                    \"city\":\t\t    \"Wilmington\",\n                    \"state\":        \"DE\",\n                    \"zip\":          \"00000\"\n                },\n            \"paymentType\":      \"stored\",\n            \"storedPaymentID\":\t1\n        }\n}"
                },
                {
                    "id": "fb5d4629-d004-49d5-9f5f-0c87357e5aa3",
                    "name": "404 - Order not found",
                    "originalRequest": {
                        "method": "GET",
                        "header": [],
                        "url": {
                            "raw": "{{ordersUrl}}/orders?id=1550861260442-MTU1MDg2MTI2MDQ0Mg==",
                            "host": [
                                "{{ordersUrl}}"
                            ],
                            "path": [
                                "orders"
                            ],
                            "query": [
                                {
                                    "key": "id",
                                    "value": "1550861260442-MTU1MDg2MTI2MDQ0Mg=="
                                }
                            ]
                        }
                    },
                    "status": "Not Found",
                    "code": 404,
                    "_postman_previewlanguage": "Text",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json"
                        }
                    ],
                    "cookie": [],
                    "responseTime": null,
                    "body": "{\n    \"status\":   \"failed\",\n    \"message\":  \"Order does not exist\"\n}"
                }
            ]
        },
        {
            "name": "Add Order",
            "id": "4ce8c07a-b01c-4ff2-aa67-80486bb65d63",
            "request": {
                "auth": {
                    "type": "apikey",
                    "apikey": [
                        {
                            "key": "key",
                            "value": "x-api-key",
                            "type": "string"
                        },
                        {
                            "key": "value",
                            "value": true,
                            "type": "boolean"
                        },
                        {
                            "key": "in",
                            "value": "header",
                            "type": "string"
                        }
                    ]
                },
                "method": "POST",
                "header": [
                    {
                        "key": "Content-Type",
                        "value": "application/json"
                    }
                ],
                "body": {
                    "mode": "raw",
                    "raw": "{\n    \"identifier\": \"nisi sed voluptate\",\n    \"name\": \"voluptate tempor dolore exercitation\",\n    \"description\": \"commodo proident anim nulla\",\n    \"image\": \"voluptate velit nostrud\",\n    \"url\": \"qui ipsum irure\",\n    \"brand\": \"incididunt proident consequat dolore\",\n    \"category\": \"nostrud fugiat eiusmod\",\n    \"color\": \"reprehenderit sit\",\n    \"logo\": \"sed qui aliqua in\",\n    \"manufacturer\": \"quis sunt ut\",\n    \"material\": \"comm\",\n    \"model\": \"ea\",\n    \"releaseDate\": \"qui laborum sed\",\n    \"sku\": \"id voluptate Duis\",\n    \"width\": \"incididunt ut\",\n    \"weight\": \"est Excepteur ea sit\",\n    \"depth\": \"cillum officia qui id\",\n    \"height\": \"minim anim labore pro\"\n}"
                },
                "url": {
                    "raw": "{{baseUrl}}/orders",
                    "host": [
                        "{{baseUrl}}"
                    ],
                    "path": [
                        "orders"
                    ]
                },
                "description": "Creates a new order."
            },
            "response": []
        },
        {
            "name": "Delete",
            "id": "8d960395-f846-421e-acd0-fc5dee7d2e8b",
            "request": {
                "auth": {
                    "type": "apikey",
                    "apikey": [
                        {
                            "key": "key",
                            "value": "x-api-key",
                            "type": "string"
                        },
                        {
                            "key": "value",
                            "value": true,
                            "type": "boolean"
                        },
                        {
                            "key": "in",
                            "value": "header",
                            "type": "string"
                        }
                    ]
                },
                "method": "DELETE",
                "header": [],
                "url": {
                    "raw": "{{baseUrl}}/orders/:orderId",
                    "host": [
                        "{{baseUrl}}"
                    ],
                    "path": [
                        "orders",
                        ":orderId"
                    ],
                    "variable": [
                        {
                            "id": "38115650-bcef-4fb5-a7d9-17bbeba29c13",
                            "key": "orderId",
                            "value": "mollit minim Lorem",
                            "description": "(Required) The order ID to retrieve."
                        }
                    ]
                },
                "description": "Delete an individual order."
            },
            "response": [
                {
                    "id": "806fac4e-34ce-43e6-be6d-f80ee1496466",
                    "name": "Order deleted.",
                    "originalRequest": {
                        "method": "DELETE",
                        "header": [
                            {
                                "description": "Added as a part of security scheme: apikey",
                                "key": "x-api-key",
                                "value": "<API Key>"
                            }
                        ],
                        "url": {
                            "raw": "{{baseUrl}}/orders/:orderId",
                            "host": [
                                "{{baseUrl}}"
                            ],
                            "path": [
                                "orders",
                                ":orderId"
                            ],
                            "variable": [
                                {
                                    "key": "orderId"
                                }
                            ]
                        }
                    },
                    "status": "No Content",
                    "code": 204,
                    "_postman_previewlanguage": "Text",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "text/plain"
                        }
                    ],
                    "cookie": [],
                    "responseTime": null,
                    "body": ""
                }
            ]
        }
    ],
    "variable": [
        {
            "id": "13f39e3a-31a6-4650-b905-304572ff6022",
            "key": "baseUrl",
            "value": "{{ordersUrl}}",
            "type": "string"
        }
    ]
}